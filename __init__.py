# -*- coding: utf-8 -*-
"""
/***************************************************************************
 SmoothLine
                                 A QGIS plugin
 Allow to smooth a single line with a clic and respects topology.
                             -------------------
        begin                : 2016-12-27
        copyright            : (C) 2016 by  Alberto De Luca for Tabacco Editrice
        email                : sergio.gollino@tabaccoeditrice.com
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load SmoothLine class from file SmoothLine.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .smooth_line import SmoothLine
    return SmoothLine(iface)
